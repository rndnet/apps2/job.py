import os
import json
from pathlib import Path

from .file_readers import file_readers

root = Path('.').resolve()
instance = int(root.name)
package_index = 0

#---------------------------------------------------------------------------
def secret(name, required=False):
    value = os.environ.get(name)
    if required and value is None:
        raise Exception(f'The required project secret "{name}" is not set')
    return value

#---------------------------------------------------------------------------
class Package:
    def __init__(self, path):
        self.path = path

    @property
    def id(self):
        return int(self.path.name)

    @property
    def label(self):
        path = self.path / 'label'
        if path.is_file():
            return path.read_text().strip()

    def files(self, *suffixes):
        return [f for f in (self.path / 'files').glob('*')
                if f.is_file() and (not suffixes or f.suffix.lower() in suffixes) ]

    @property
    def meta(self):
        path = self.path / 'meta.json'
        if path.is_file():
            return json.loads(path.read_text())
        return {}

    def load(self, readers={}):
        data = self.meta
        readers = {**file_readers, **readers}

        for f in self.files():
            reader = readers.get(f.suffix.lower())
            if reader:
                data[f.stem] = reader(f)
            else:
                print(f'Skipping {f}: unknown format')

        return data

#---------------------------------------------------------------------------
def params():
    path = root / 'in' / 'params.json'
    if path.is_file():
        return json.loads(path.read_text())
    return {}

#---------------------------------------------------------------------------
def packages():
    return [Package(p) for p in (root / 'in').glob('*') if p.is_dir()]

#---------------------------------------------------------------------------
def files(*suffixes):
    for p in packages():
        for f in p.files(*suffixes):
            yield f

#---------------------------------------------------------------------------
def load(readers={}):
    data = params()

    for p in packages():
        data.update(p.load(readers))

    return data

#---------------------------------------------------------------------------
def save_hdf5(path, data):
    import h5py
    import numpy

    with h5py.File(path, 'w') as f:
        f.create_dataset(path.stem, data=data, track_times=False,
                compression='gzip' if isinstance(data, numpy.ndarray) else None)

#---------------------------------------------------------------------------
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        import numpy as np

        if isinstance(obj, np.generic):
            return obj.item()

        return super(NumpyEncoder, self).default(obj)

#---------------------------------------------------------------------------
def save_package(label=None, data={}, meta={}, image={}):
    global package_index

    package_index += 1
    path = root / 'out' / str(package_index)
    path.mkdir(parents=True, exist_ok=True)

    if label is not None:
        (path / 'label').write_text(str(label).strip())

    if meta:
        if not isinstance(meta, dict):
            raise ValueError('meta parameter should be a dictionary')

        (path / 'meta.json').write_text(json.dumps(meta, ensure_ascii=False, cls=NumpyEncoder))

    if data or image:
        (path / 'files').mkdir(parents=True, exist_ok=True)

    for k,v in data.items():
        f = path / 'files' / k

        if callable(v):
            v(f)
        else:
            save_hdf5(f.with_suffix('.h5'), v)

    for k,v in image.items():
        f = path / 'files' / k
        if callable(v):
            v(f)
        elif v.__module__.startswith('matplotlib'):
            if not f.suffix:
                f = f.with_suffix('.png')
            v.savefig(f, transparent=True)
        elif v.__module__.startswith('plotly'):
            v.write_json(str(f.with_suffix('.plt')))

        else:
            print(f'Skipped image {k}: unknown format')

    return path
