import os
import json
import requests
import urllib3
import ssl
import functools
import hashlib
import pathlib
import mimetypes

from datetime import datetime
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from binaryornot.check import is_binary
from sseclient import SSEClient

urllib3.disable_warnings()
ssl._create_default_https_context = ssl._create_unverified_context

#---------------------------------------------------------------------------
def timestamp():
    return datetime.utcnow().replace(microsecond=0).isoformat(sep=' ')

#---------------------------------------------------------------------------
def response_json(fn):
    @functools.wraps(fn)
    def wrapper(*args, **kwargs):
        r = fn(*args, **kwargs)
        if r.status_code != requests.codes.ok:
            print(r.text)
        r.raise_for_status()
        return r.json()
    return wrapper

#---------------------------------------------------------------------------
def file_hash(path):
    chunk = 65536

    h = hashlib.sha256()
    with open(path, 'rb') as f:
        for b in iter(lambda: f.read(chunk), b''):
            h.update(b)

    return h.hexdigest()

#---------------------------------------------------------------------------
class Server:
    def __init__(self, base_url=None, apikey=None):
        if base_url is None:
            base_url = os.environ.get('RNDNET_API_SERVER')
            assert base_url, 'API server URL is not set'

        self.base_url = f'{base_url}/api'

        self.session = requests.Session()
        self.raw_session = requests.Session()

        # https://www.peterbe.com/plog/best-practice-with-retries-with-requests
        adapter = HTTPAdapter(max_retries=Retry(
            total=3, read=3, connect=3,
            backoff_factor=0.3, status_forcelist=(502,504)))

        for session in (self.session, self.raw_session):
            session.mount('http://', adapter)
            session.mount('https://', adapter)
            session.verify = False

        self.tokens = {}

        if apikey is not None:
            self.tokens['refresh_token'] = apikey
            self.refresh_url = f'{self.base_url}/auth/refresh'
        else:
            self.tokens['refresh_token'] = os.environ.get('RNDNET_REFRESH_TOKEN')
            self.refresh_url = f'{self.base_url}/schedulers/auth/refresh'

        self.refresh()
        self.session.hooks['response'].append(self.refresh_and_retry)

    @property
    def access_header(self):
        return dict(Authorization=f'Bearer {self.tokens["access_token"]}')

    @property
    def refresh_header(self):
        return dict(Authorization=f'Bearer {self.tokens["refresh_token"]}')

    def refresh(self):
        r = self.raw_session.post(self.refresh_url,
                headers=self.refresh_header, data=dict(tokens=True))
        r.raise_for_status()
        self.tokens.update(r.json())

        self.session.headers.update(self.access_header)

    def refresh_and_retry(self, response, *args, **kwargs):
        if response.status_code == requests.codes.unauthorized and 'refresh_token' in self.tokens:
            self.refresh()

            request = response.request
            request.headers.update(self.access_header)

            return self.session.send(request)

    @response_json
    def get(self, resource, *args, **kwargs):
        return self.session.get(f'{self.base_url}{resource}', *args, **kwargs)
        
    @response_json
    def post(self, resource, *args, **kwargs):
        return self.session.post(f'{self.base_url}{resource}', *args, **kwargs)
        
    @response_json
    def put(self, resource, *args, **kwargs):
        return self.session.put(f'{self.base_url}{resource}', *args, **kwargs)

    @response_json
    def delete(self, resource, *args, **kwargs):
        return self.session.delete(f'{self.base_url}{resource}', *args, **kwargs)

    def download(self, path, file):
        path = pathlib.Path(path) / file['name']
        path.parent.mkdir(parents=True, exist_ok=True)

        print(f'[{timestamp()}] Downloading {path}...')

        ntries = 2

        while True:
            r = self.raw_session.get(file['link'], stream=True)
            r.raise_for_status()

            h = hashlib.sha256()
            with open(path, 'wb') as f:
                for chunk in r:
                    h.update(chunk)
                    f.write(chunk)

            ntries -= 1

            if h.hexdigest() == file['hash']:
                break
            elif ntries > 0:
                print(f'{path}: wrong content checksum. retrying...')
            else:
                raise Exception(f'{path}: wrong content checksum.')

        if file['executable']:
            os.chmod(path, 0o770)

    def upload_project_file(self, project, path, name=None):
        path     = pathlib.Path(path)
        name     = name or path.name
        size     = path.stat().st_size
        f_hash   = file_hash(path)
        binary   = is_binary(str(path))
        f_type,_ = mimetypes.guess_type(str(path))
        if f_type is None:
            f_type = 'application/x-binary' if binary else 'text/plain'

        link = self.put(f'/projects/{project}/objects/{f_hash}')
        if link is not None:
            with open(path, 'rb') as f:
                self.raw_session.put(link, data=f, headers={
                    'Content-Type': f_type,
                    'Content-Length': str(size)
                    }).raise_for_status()

        return dict(
            name       = name,
            hash       = f_hash,
            type       = f_type,
            executable = os.access(path, os.X_OK),
            is_binary  = binary,
            size       = size
            )

    def events(self, resource):
        for event in SSEClient(f'{self.base_url}{resource}',
                session=self.session,
                headers={
                    'Cache-Control': 'no-cache',
                    'Connection': 'keep-alive',
                    }):
            yield json.loads(event.data)
