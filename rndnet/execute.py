import os
import sys
import json
import subprocess
import mimetypes
import threading
import traceback
import contextlib
import time

from pathlib import Path
from collections import deque
from itertools import chain
from datetime import datetime, timedelta
from binaryornot.check import is_binary
from .server import Server, file_hash, timestamp

HEARTBEAT_INTERVAL = timedelta(seconds=60)

#---------------------------------------------------------------------------
def download_instance(server, instance, root, log):
    with open(log, 'at', buffering=1) as log_file:
        with contextlib.redirect_stdout(log_file):
            (root / 'in').mkdir(parents=True, exist_ok=True)

            (root / 'in' / 'params.json').write_text(
                    json.dumps(
                        {m['name'] : m['value'] for m in instance['meta']},
                        ensure_ascii=False))

            files = {root / f['name']: (root, f)
                    for f in instance['files']
                    if Path(f['name']).parts[0] != 'out'}
            files.update({root / f['name']: (root, f) for f in instance['node']['files']})

            for p in instance['packages']:
                mapping = {m['var_in'] : m['var_out'] for m in p['mapping']}

                for package in chain([p['package']], p['package']['children']):
                    path = root / 'in' / str(package['id'])
                    path.mkdir(parents=True, exist_ok=True)

                    for f in package['files']:
                        name = Path(f['name'])
                        f['name'] = name.with_name(mapping.get(name.stem, name.stem)).with_suffix(name.suffix)
                        files[path / 'files' / f['name']] = (path / 'files', f)

                    if package['label'] is not None:
                        (path / 'label').write_text(package['label'])

                    (path / 'meta.json').write_text(
                            json.dumps({
                                mapping.get(m['name'], m['name']) : m['value']
                                for m in package['meta']
                                }, ensure_ascii=False))

            for p,f in files.values():
                server.download(p, f)

            print(f'[{timestamp()}] Input data downloaded')
            return root

#---------------------------------------------------------------------------
def execute_instance(server, instance, root):
    env = os.environ.copy()
    env.pop('RNDNET_REFRESH_TOKEN')

    base_url = os.environ.get('JUPYTER_BASE_URL')

    if instance['interactive'] and base_url:
        script = f"jupyter notebook --allow-root --no-browser --ip='*' --NotebookApp.base_url={base_url} --NotebookApp.token=''"
    else:
        script = instance['node']['script'] or "echo 'Empty script: nothing to do :('\nexit 1"

    script_wrapper = f"""(
{script}
) 2>&1 | ts "[%Y-%m-%d %H:%M:%S]" | tee -a {instance["id"]}.log
rc=${{PIPESTATUS[0]}}
exit $rc
"""

    p = subprocess.run(script_wrapper, cwd=root, shell=True, executable="/bin/bash")
    return p.returncode

#---------------------------------------------------------------------------
def upload_instance(server, instance, status, root, log):
    with open(log, 'at', buffering=1) as log_file:
        with contextlib.redirect_stdout(log_file):
            print(f'[{timestamp()}] Uploading instance output to server...')

    exclude_dirs = ('in', '__pycache__', '.ipynb_checkpoints')
    def enumerate_files():
        for dir, dirs, files in os.walk(root):
            path = Path(dir)
            dirs[:] = [d for d in dirs
                if (path / d).relative_to(root).parts[0] not in exclude_dirs]
            for f in files:
                yield path / f

    def upload_files(paths):
        p2h = {Path(path) : file_hash(path) for path in paths}
        h2p = {h : p for p,h in p2h.items()}
        links  = server.post(f'/schedulers/instances/{instance["id"]}/objects', data={
            'object_id': list(h2p.keys()) })

        for item in links:
            path = h2p[item['object_id']]
            link = item['link']

            binary = is_binary(str(path))
            type,_ = mimetypes.guess_type(str(path))
            if type is None:
                type = 'application/x-binary' if binary else 'text/plain'

            if link is not None:
                with open(path, 'rb') as f:
                    server.raw_session.put(link, data=f, headers={
                        'Content-Type': type,
                        'Content-Length': str(path.stat().st_size)
                        }).raise_for_status()


        files = []
        for path,h in p2h.items():
            binary = is_binary(str(path))
            type,_ = mimetypes.guess_type(str(path))
            if type is None:
                type = 'application/x-binary' if binary else 'text/plain'

            files.append(dict(
                name       = str(path.relative_to(root)),
                type       = type,
                hash       = h,
                executable = os.access(path, os.X_OK),
                is_binary  = binary,
                size       = path.stat().st_size
                ))

        return files

    server.put(f'/schedulers/instances/{instance["id"]}', json={
        'status': str(status),
        'files': upload_files(enumerate_files())
        })

#---------------------------------------------------------------------------
def heartbeat(server, instance_id, log, done):
    def send_heartbeat():
        try:
            tail = ''.join(deque(open(log), 100)) if log.is_file() else ''
            server.put(f'/schedulers/instances/{instance_id}',
                    json=dict(log_tail=tail, heartbeat=True))
        except:
            print(traceback.format_exc())

    chkpt = None
    while not done.is_set():
        time.sleep(0.1)
        if chkpt is None or datetime.utcnow() >= chkpt:
            send_heartbeat()
            chkpt = datetime.utcnow() + HEARTBEAT_INTERVAL

#---------------------------------------------------------------------------
def main():
    import argparse
    from getpass import getpass

    parser = argparse.ArgumentParser()
    parser.add_argument('-s,--server',   dest='server',   required=True)
    parser.add_argument('-i,--instance', dest='instance', required=True, type=int)
    args = parser.parse_args()

    if 'RNDNET_REFRESH_TOKEN' not in os.environ:
        raise Exception('Access token not found in environment')

    server = Server(args.server)
    instance = server.get(f'/schedulers/instances/{args.instance}')
    
    root = Path(f'{instance["id"]}').resolve()
    root.mkdir(parents=True, exist_ok=True)

    log = root / f'{instance["id"]}.log'

    done = threading.Event()
    beat = threading.Thread(target=heartbeat, args=(server, instance['id'], log, done))
    beat.start()

    try:
        download_instance(server, instance, root, log)
        status = execute_instance(server, instance, root)
        upload_instance(server, instance, status, root, log)
    finally:
        done.set()
        beat.join()

