from setuptools import setup, find_packages

setup(
        name='rndnet_job',
        author='MR EVGENII BIRIALTCEV, SOLE TRADE',
        author_email='mail@rndnet.net',
        version='0.1.0',
        description='Support module for python job scripts for Rndnet.net cloud',
        include_package_data=True,
        packages=find_packages(),
        entry_points={
            'console_scripts': [
                'rndnet_execute=rndnet.execute:main'
                ]
            },
        )
